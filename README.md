# Get Started

1. **Install required dependencies**

    `npm install`    

2. **Run the app**

    `npm start -s`

3. **Build the app**

    `npm run build`
    This will create a dist folder containing a production build and also start a dev server under localhost

## Initial Machine Setup

1. **Install [Node 4.0.0 or greater](https://nodejs.org)**

    (5.0 or greater is recommended for optimal build performance)
    Need to run multiple versions of Node? Use [nvm](https://github.com/creationix/nvm).

2. **[Disable safe write in your editor](https://webpack.js.org/guides/development/#adjusting-your-text-editor)** to assure hot reloading works properly.

3. Complete the steps below for your operating system:

    ### macOS

    * Install [watchman](https://facebook.github.io/watchman/) via `brew install watchman` or fswatch via `brew install fswatch` to avoid [this issue](https://github.com/facebook/create-react-app/issues/871) which occurs if your macOS has no appropriate file watching service installed.

    ### Linux

    * Run this to [increase the limit](http://stackoverflow.com/questions/16748737/grunt-watch-error-waiting-fatal-error-watch-enospc) on the number of files Linux will watch.

        `echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`.

    ### Windows
    
    * **Install [Python 2.7](https://www.python.org/downloads/)**. Some node modules may rely on node-gyp, which requires Python on Windows.
    * **Install C++ Compiler**. Browser-sync requires a C++ compiler on Windows.
    
      [Visual Studio Express](https://www.visualstudio.com/en-US/products/visual-studio-express-vs) comes bundled with a free C++ compiler.
      
      If you already have Visual Studio installed:
      Open Visual Studio and go to File -> New -> Project -> Visual C++ -> Install Visual C++ Tools for Windows Desktop.
      The C++ compiler is used to compile browser-sync (and perhaps other Node modules).

---

## Technologies

Blueweather assignment application was developed using the following technologies:

* React
* Redux
* React Router
* Babel
* Webpack
* Browsersync
* Jest
* ESLint
* SASS
* npm Scripts

---

## Explanation of design decisions

1. **Redux Usage**

    Redux was selected due to its efficiency in managing state and handling user actions.     

2. **Project structure**

    A basic form and number of directories and files has been used taking into account the simplicity of the application. In real-complex applications I prefer to use a modular approach that separates entities by individual functionality rather than generic categorization

3. **Styling**

    For this assignment I have used bootstrap 3 as requested in the assignment documentation. It must be noted that only the css part of bootstrap has been included as I consider leaving jquery out of ReactJS based projects as a best practice.  
    SASS is the tool of choice for CSS pre-processing due the added efficiency it brings with the usage of variables, mixins and modular capabilities (Developer can separate styles into different entities and include them into bigger ones)
    
4. **Testing**

    A minor test has been added that merely checks the primary component WeatherContainer for the inclusion of the dumb component CitiesList.  
  
5. **API calls efficiency**

    A thought for future development would be to keep fetched data from the API locally or perhaps in the sessionStorage for a specific ammount of time in order to prevent unnecessary calls.   
