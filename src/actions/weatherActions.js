import * as types from "../actions/actionTypes";
import axios from "axios";
import * as constants from "../utils/constants";

export function loadCitiesSuccess(cities) {
  return {type: types.LOAD_CITIES_SUCCESS, cities};
}

export function loadCities() {
  return (dispatch => {
    return (
      dispatch(loadCitiesSuccess(constants.cities_array))
    );
  })
}

export function loadWeatherSuccess(weather) {
  return {type: types.LOAD_WEATHER_SUCCESS, weather};
}

export function loadWeather(state, alias) {
  return (dispatch => {
    return axios.get('http://api.wunderground.com/api/'+ constants.API_KEY +'/history_20171030/q/'+ state +'/'+ alias +'.json').then(weather => {
      dispatch(loadWeatherSuccess(weather.data));
    }).catch(error => {
      throw(error);
    });
  })
}

export function clearWeather() {
  return {type: types.CLEAR_WEATHER};
}

/* filter experiences according to searchText */
export function filterCities(filteredCities) {
  return {type: types.FILTER_CITIES, filteredCities};
}

/* weather modal actions */
export const openWeatherModal = (data) => {
  return {
    type: types.OPEN_WEATHER_MODAL,
    data
  };
};

export const closeWeatherModal = () => {
  return {type: types.CLOSE_WEATHER_MODAL};
};
