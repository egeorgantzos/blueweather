export const API_KEY = "925f8b99d34a87e8";

export const cities_array = [
  {
    "id": 1,
    "name": "New York",
    "state": "NY",
    "alias": "New_York",
    "thumbUrl": require("../styles/images/ny.jpg"),
  },
  {
    "id": 2,
    "name": "San Francisco",
    "state": "CA",
    "alias": "San_Francisco",
    "thumbUrl": require("../styles/images/sanfran.jpg"),
  },
  {
    "id": 3,
    "name": "Chicago",
    "state": "IL",
    "alias": "Chicago",
    "thumbUrl": require("../styles/images/chicago.jpg"),
  },
  {
    "id": 4,
    "name": "Los Angeles",
    "state": "CA",
    "alias": "Los_Angeles",
    "thumbUrl": require("../styles/images/la.png"),
  },
  {
    "id": 5,
    "name": "Detroit",
    "state": "MI",
    "alias": "Detroit",
    "thumbUrl": require("../styles/images/detroit.jpg"),
  },
];
