import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import AutoComplete from 'material-ui/AutoComplete';

const CitiesList = (props) => {

  const style = {
    width: "100%",
    overflow: "hidden",
    marginBottom: 4,
  };

  const cities = props.cities;
  const cityNames = props.cityNames;
  const handleSearchClick = props.handleSearchClick;
  const title = props.weatherModalProps.modalProps.title;
  const modalContent = (flag) => {
    if (flag === '') {
      return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-6">
              <div className="loader"></div>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-6">
              Max percentage humidity
            </div>
            <div className="col-xs-6">
              {props.maxhumidity}
            </div>
          </div>
          <div className="row">
            <div className="col-xs-6">
              Max Temp in C
            </div>
            <div className="col-xs-6">
              {props.maxtempm}
            </div>
          </div>
          <div className="row">
            <div className="col-xs-6">
              Min Temp in C
            </div>
            <div className="col-xs-6">
              {props.mintempm}
            </div>
          </div>
          <div className="row">
            <div className="col-xs-6">
              Precipitation in mm
            </div>
            <div className="col-xs-6">
              {props.precipm}
            </div>
          </div>
        </div>
      )
    }
  };

  return (
    <div className="weather">
      <div className="weather__header">
        <img src={require('../styles/images/thebluelogo-white.png')} />
      </div>
      <div className="weather__wrapper">
        <div className="weather__cities cities">
          <div className="cities__title">
            <h1>CITY WEATHER</h1>
          </div>
          <div className="cities__body">
            <div className="cities__search">
              <AutoComplete
                hintText="Search for city"
                floatingLabelText="Search for city"
                dataSource={cityNames}
                filter={AutoComplete.caseInsensitiveFilter}
                onUpdateInput={handleSearchClick}
                onNewRequest={handleSearchClick}
              />
            </div>
            <div className="cities__list">
              {cities.map((city, key) => (
                <Paper className="city" key={key} style={style} zDepth={2}>
                  <a className="city__link" onClick={()=>{props.handleCityClick({
                    isOpen: true,
                    title: city.name,
                    state: city.state,
                    alias: city.alias,
                  })}}>
                    <img className="city__thumb" src={city.thumbUrl} />
                    <div className="city__title">
                      <h2>{city.name}</h2>
                    </div>
                  </a>
                </Paper>
              ))}
            </div>
            <Dialog
              title={title}
              modal={false}
              open={props.weatherModalProps.modalProps.isOpen}
              onRequestClose={props.closeModal}>
              {modalContent(props.maxtempm)}
            </Dialog>
          </div>
        </div>
      </div>
    </div>
  );
};

CitiesList.propTypes = {
  cities: PropTypes.array.isRequired,
  cityNames: PropTypes.array.isRequired,
  maxhumidity: PropTypes.string.isRequired,
  maxtempm: PropTypes.string.isRequired,
  mintempm: PropTypes.string.isRequired,
  precipm: PropTypes.string.isRequired,
  handleSearchClick: PropTypes.func.isRequired,
  weatherModalProps: PropTypes.shape({
    modalProps: PropTypes.shape({
      isOpen: PropTypes.bool.isRequired,
      title: PropTypes.string.isRequired,
      state: PropTypes.string.isRequired,
      alias: PropTypes.string.isRequired,
    }),
  }),
  closeModal: PropTypes.func.isRequired,
};

export default CitiesList;
