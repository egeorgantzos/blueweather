import React from "react";
import { shallow } from "enzyme";
import {WeatherContainer} from "./WeatherContainer";
import CitiesList from "../CitiesList";
describe("<WeatherContainer />", () => {
  const actions = {
    loadCities: jest.fn(),
    loadWeather: jest.fn(),
    clearWeather: jest.fn(),
    filterCities: jest.fn(),
    openWeatherModal: jest.fn(),
    closeWeatherModal: jest.fn(),
  };
  const cities = [];
  const weather = {};
  const weatherModalProps = {};
  const filteredCities = [];

  it("should contain <CitiesList />", () => {
    const wrapper = shallow(
      <WeatherContainer
        cities = {cities}
        weather = {weather}
        weatherModalProps = {weatherModalProps}
        filteredCities = {filteredCities}
        actions = {actions}
      />
    );

    expect(wrapper.find(CitiesList).length).toEqual(1);
  });

});

