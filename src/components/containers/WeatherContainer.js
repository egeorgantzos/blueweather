import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import PropTypes from 'prop-types';
import CitiesList from '../CitiesList';
import * as weatherActions from '../../actions/weatherActions';

export class WeatherContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {cities} = this.props;
    const filteredCities = this.props.filteredCities.length > 0
      ?this.props.filteredCities
      :this.props.cities;
    const cityNames = cities.map(i => i.name);
    const handleSearchClick = (searchText) => {
      let newFilteredCities = cities.filter(j => {
        if (j.name.toLowerCase().includes(searchText.toLowerCase())){
          return j;
        }
      });
      this.props.actions.filterCities(newFilteredCities);
    };
    // loadWeather
    const handleCityClick = (data) => {
      this.props.actions.loadWeather(data.state, data.alias);
      this.props.actions.openWeatherModal(data);
    };
    // closeModal
    const handleCloseModal = () => {
      this.props.actions.clearWeather();
      this.props.actions.closeWeatherModal();
    };

    const maxhumidity = this.props.weather.history
    ? this.props.weather.history.dailysummary[0].maxhumidity
      : '';
    const maxtempm = this.props.weather.history
      ? this.props.weather.history.dailysummary[0].maxtempm
      : '';
    const mintempm = this.props.weather.history
      ? this.props.weather.history.dailysummary[0].mintempm
      : '';
    const precipm = this.props.weather.history
     ? this.props.weather.history.dailysummary[0].precipm
      : '';




    return (
      <CitiesList
        cities = {filteredCities}
        maxhumidity = {maxhumidity}
        maxtempm = {maxtempm}
        mintempm = {mintempm}
        precipm = {precipm}
        cityNames = {cityNames}
        closeModal = {handleCloseModal}
        handleSearchClick={handleSearchClick}
        handleCityClick = {handleCityClick}
        weatherModalProps={this.props.weatherModalProps}
      />
    );
  }
}

WeatherContainer.propTypes = {
  cities: PropTypes.array.isRequired,
  weather: PropTypes.object.isRequired,
  filteredCities: PropTypes.array.isRequired,
  actions: PropTypes.shape({
    filterCities: PropTypes.func.isRequired,
    closeWeatherModal: PropTypes.func.isRequired,
    openWeatherModal: PropTypes.func.isRequired,
    loadWeather: PropTypes.func.isRequired,
    clearWeather: PropTypes.func.isRequired,
  }),
  weatherModalProps: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    cities: state.cities,
    weather: state.weather,
    filteredCities: state.filteredCities,
    weatherModalProps: state.weatherModalProps,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({}, weatherActions), dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WeatherContainer);
