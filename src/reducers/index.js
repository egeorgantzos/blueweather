import { combineReducers } from 'redux';
import weather from './weatherReducer';
import cities from './citiesReducer';
import filteredCities from './filterCitiesReducer';
import weatherModalProps from './weatherModalReducer';

const rootReducer = combineReducers({
  weather,
  cities,
  filteredCities,
  weatherModalProps,
});

export default rootReducer;
