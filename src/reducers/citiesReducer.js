import * as types from '../actions/actionTypes';

const weatherReducer = (state = [], action) => {
  switch(action.type) {
    case types.LOAD_CITIES_SUCCESS:
      return action.cities;

    default:
      return state;
  }
};

export default weatherReducer;
