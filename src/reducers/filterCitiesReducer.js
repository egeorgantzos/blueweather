import * as types from '../actions/actionTypes';

const filterCitiesReducer = (state = [], action) => {
  switch(action.type) {
    case types.FILTER_CITIES:
      return action.filteredCities;

    default:
      return state;
  }
};

export default filterCitiesReducer;
