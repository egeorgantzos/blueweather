import * as types from "../actions/actionTypes";

const initialState = {
  modalProps: {
    isOpen: false,
    title: '',
    state: '',
    alias: '',
  }
};

export default function  weatherModalReducer(state = initialState, action) {
  switch (action.type) {
    case types.OPEN_WEATHER_MODAL:
      return {
        modalProps: action.data
      };
    case types.CLOSE_WEATHER_MODAL:
      return initialState;
    default:
      return state;
  }
}
