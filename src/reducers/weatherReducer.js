import * as types from '../actions/actionTypes';

const initialState = {
  history: {
    dailysummary: [
      {
        maxhumidity: '',
        maxtempm: '',
        mintempm: '',
        precipm: '',
      }
    ]
  }
};

const weatherReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.LOAD_WEATHER_SUCCESS:
      return action.weather;
    case types.CLEAR_WEATHER:
      return initialState;

    default:
      return state;
  }
};

export default weatherReducer;
