import React from 'react';
import {render} from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import configureStore, {history} from './store/configureStore';
import {Provider} from 'react-redux';
import App from './components/App';
import {loadCities} from './actions/weatherActions';
import './styles/styles.scss';

require('./favicon.ico'); // Tell webpack to load favicon.ico
const store = configureStore();
store.dispatch(loadCities());

render(
  <Provider store={store}>
    <AppContainer>
      <App store={store} history={history}/>
    </AppContainer>
  </Provider>,
  document.getElementById('app')
);
